Google Style Guides (Modified)
===================

Modifications to this Style Guide for growingSTEMS use have only been made to the [Java Style Guide][java].

Every major open-source project has its own style guide: a set of conventions
(sometimes arbitrary) about how to write code for that project. It is much
easier to understand a large codebase when all the code in it is in a
consistent style.

“Style” covers a lot of ground, from “use camelCase for variable names” to
“never use global variables” to “never use exceptions.” This project
([growingstems/styleguide](https://gitlab.com/growingstems/styleguide)) links to the
style guidelines we use for growingSTEMS code. If you are modifying a project that
originated at growingSTEMS, you may be pointed to this page to see the style guides
that apply to that project.

This project holds the [C++ Style Guide][cpp], [Swift Style Guide][swift], [Objective-C Style Guide][objc],
[Java Style Guide][java], [Python Style Guide][py], [R Style Guide][r],
[Shell Style Guide][sh], [HTML/CSS Style Guide][htmlcss],
[JavaScript Style Guide][js], [AngularJS Style Guide][angular],
[Common Lisp Style Guide][cl], and [Vimscript Style Guide][vim]. This project
also contains [cpplint][cpplint], a tool to assist with style guide compliance,
and [google-c-style.el][emacs], an Emacs settings file for Google style.

If your project requires that you create a new XML document format, the [XML
Document Format Style Guide][xml] may be helpful. In addition to actual style
rules, it also contains advice on designing your own vs. adapting an existing
format, on XML instance document formatting, and on elements vs. attributes.

The style guides in this project are licensed under the CC-By 3.0 License,
which encourages you to share these documents.
See [https://creativecommons.org/licenses/by/3.0/][ccl] for more details.

The following Google style guides live outside of this project:
[Go Code Review Comments][go] and [Effective Dart][dart].

<a rel="license" href="https://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/3.0/88x31.png" /></a>

[cpp]: https://growingstems.gitlab.io/styleguide/cppguide.html
[swift]: https://growingstems.gitlab.io/swift/
[objc]: objcguide.md
[java]: https://growingstems.gitlab.io/styleguide/javaguide.html
[py]: https://growingstems.gitlab.io/styleguide/pyguide.html
[r]: https://growingstems.gitlab.io/styleguide/Rguide.html
[sh]: https://growingstems.gitlab.io/styleguide/shellguide.html
[htmlcss]: https://growingstems.gitlab.io/styleguide/htmlcssguide.html
[js]: https://growingstems.gitlab.io/styleguide/jsguide.html
[angular]: https://growingstems.gitlab.io/styleguide/angularjs-google-style.html
[cl]: https://growingstems.gitlab.io/styleguide/lispguide.xml
[vim]: https://growingstems.gitlab.io/styleguide/vimscriptguide.xml
[cpplint]: https://gitlab.com/growingstems/styleguide/tree/gh-pages/cpplint
[emacs]: https://raw.gitlabusercontent.com/growingstems/styleguide/gh-pages/google-c-style.el
[xml]: https://growingstems.gitlab.io/styleguide/xmlstyle.html
[go]: https://golang.org/wiki/CodeReviewComments
[dart]: https://www.dartlang.org/guides/language/effective-dart
[ccl]: https://creativecommons.org/licenses/by/3.0/
